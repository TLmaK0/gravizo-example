# [Gravizo](http://www.gravizo.com)#
### How to include graphviz graphs in bitbucket README.md ###
(look at http://www.gravizo.com for more information)

![Alt text](http://g.gravizo.com/source/gravizosample2?https%3A%2F%2Fbitbucket.org%2FTLmaK0%2Fgravizo-example%2Fraw%2Fmaster%2FREADME.md#
gravizosample2
 digraph G {
   main -> parse -> execute
   main -> init
   main -> cleanup
   execute -> make_string
   execute -> printf
   init -> make_string
   main -> printf
   execute -> compare
 }
gravizosample2
)

We need to use an image to hide gravizo code. I don't find any other way to hide code. Use a blank image and #. Then put your code inside.

```
![Alt text](http://g.gravizo.com/source/gravizosample?https%3A%2F%2Fbitbucket.org%2FTLmaK0%2Fgravizo-example%2Fraw%2Fmaster%2FREADME.md#
gravizosample
 digraph G {
   main -> parse -> execute
   main -> init
   main -> cleanup
   execute -> make_string
   execute -> printf
   init -> make_string
   main -> printf
   execute -> compare
 }
gravizosample
)
```

![Alt text](https://upload.wikimedia.org/wikipedia/commons/c/ca/1x1.png#
gravizosample
 digraph G {
   main -> parse -> execute
   main -> init
   main -> cleanup
   execute -> make_string
   execute -> printf
   init -> make_string
   main -> printf
   execute -> compare
 }
gravizosample
}
)

You can, of course, reference another file in the repo

![Alt text](http://g.gravizo.com/source?https%3A%2F%2Fbitbucket.org%2FTLmaK0%2Fgravizo-example%2Fraw%2Fmaster%2Fsource.uml)

```
![Alt text](http://g.gravizo.com/source?https%3A%2F%2Fbitbucket.org%2FTLmaK0%2Fgravizo-example%2Fraw%2Fmaster%2Fsource.uml)
```